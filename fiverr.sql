-- Adminer 4.8.1 MySQL 8.0.31 dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `BinhLuan`;
CREATE TABLE `BinhLuan` (
  `id` int NOT NULL AUTO_INCREMENT,
  `maCongViec` int NOT NULL,
  `maNguoiBinhLuan` int NOT NULL,
  `ngayBinhLuan` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `noiDung` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `saoBinhLuan` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `maNguoiBinhLuan` (`maNguoiBinhLuan`),
  KEY `maCongViec` (`maCongViec`),
  CONSTRAINT `BinhLuan_ibfk_1` FOREIGN KEY (`maNguoiBinhLuan`) REFERENCES `NguoiDung` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `BinhLuan_ibfk_2` FOREIGN KEY (`maCongViec`) REFERENCES `CongViec` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


DROP TABLE IF EXISTS `ChiTietLoaiCongViec`;
CREATE TABLE `ChiTietLoaiCongViec` (
  `id` int NOT NULL AUTO_INCREMENT,
  `tenChiTiet` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `hinhAnh` varchar(255) DEFAULT NULL,
  `maNhomChiTietLoaiCongViec` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `maNhomChiTietLoaiCongViec` (`maNhomChiTietLoaiCongViec`),
  CONSTRAINT `ChiTietLoaiCongViec_ibfk_2` FOREIGN KEY (`maNhomChiTietLoaiCongViec`) REFERENCES `NhomChiTietLoai` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `ChiTietLoaiCongViec` (`id`, `tenChiTiet`, `hinhAnh`, `maNhomChiTietLoaiCongViec`) VALUES
(20,	'Graphis',	'Hinh anh 4',	8);

DROP TABLE IF EXISTS `CongViec`;
CREATE TABLE `CongViec` (
  `id` int NOT NULL AUTO_INCREMENT,
  `tenCongViec` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `danhGia` int NOT NULL,
  `giaTien` int NOT NULL,
  `hinhAnh` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `moTa` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `moTaNgan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `saoCongViec` int NOT NULL,
  `maChiTietLoaiCongViec` int NOT NULL,
  `nguoiTao` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `nguoiTao` (`nguoiTao`),
  KEY `saoCongViec` (`saoCongViec`),
  KEY `maChiTietLoaiCongViec` (`maChiTietLoaiCongViec`),
  CONSTRAINT `CongViec_ibfk_1` FOREIGN KEY (`nguoiTao`) REFERENCES `NguoiDung` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `CongViec_ibfk_3` FOREIGN KEY (`maChiTietLoaiCongViec`) REFERENCES `ChiTietLoaiCongViec` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


DROP TABLE IF EXISTS `LoaiCongViec`;
CREATE TABLE `LoaiCongViec` (
  `id` int NOT NULL AUTO_INCREMENT,
  `tenLoaiCongViec` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


DROP TABLE IF EXISTS `NguoiDung`;
CREATE TABLE `NguoiDung` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `birthday` varchar(20) DEFAULT NULL,
  `gender` enum('male','female','unknow') DEFAULT NULL,
  `role` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT 'user',
  `skill` json DEFAULT NULL,
  `certification` json DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


DROP TABLE IF EXISTS `NhomChiTietLoai`;
CREATE TABLE `NhomChiTietLoai` (
  `id` int NOT NULL AUTO_INCREMENT,
  `tenNhomChiTietLoaiCongViec` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `maChiTietLoai` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `maChiTietLoai` (`maChiTietLoai`),
  CONSTRAINT `NhomChiTietLoai_ibfk_4` FOREIGN KEY (`maChiTietLoai`) REFERENCES `LoaiCongViec` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


DROP TABLE IF EXISTS `ThueCongViec`;
CREATE TABLE `ThueCongViec` (
  `id` int NOT NULL AUTO_INCREMENT,
  `maCongViec` int NOT NULL,
  `maNguoiThue` int NOT NULL,
  `ngayThue` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `hoanThanh` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `maNguoiThue` (`maNguoiThue`),
  KEY `maCongViec` (`maCongViec`),
  CONSTRAINT `ThueCongViec_ibfk_1` FOREIGN KEY (`maNguoiThue`) REFERENCES `NguoiDung` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ThueCongViec_ibfk_2` FOREIGN KEY (`maCongViec`) REFERENCES `CongViec` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


-- 2023-04-05 09:17:45
