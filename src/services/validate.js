// Kiểm tra người dùng có tồn tại

const { failCode } = require("../config/response");

const checkEmpty = (res, data, message) => {
  if (data) {
    return true;
  } else {
    failCode(res, data, message);
  }
};

module.exports = { checkEmpty };
