const express = require("express");
const app = express();
app.use(express.json());
const cookieParser = require("cookie-parser");
app.use(cookieParser());

app.use(express.static("."));

const cors = require("cors");
app.use(cors());

app.listen(3000);
const rootRoute = require("./Routes/rootRoute");
app.use("/api/capstone-fiverr", rootRoute);
