const express = require("express");
const { verifyToken } = require("../utils/jwt");
const authRoute = require("./authRoute");
const commentRoute = require("./commentRoute");
const workTypeDetailRoute = require("./workTypeDetailRoute");
const workTypeRoute = require("./workTypeRoute");
const workRoute = require("./workRoute");
const userRoute = require("./userRoute");
const hireWorkRoute = require("./hireWorkRoute");
const rootRoute = express.Router();

rootRoute.use("/auth", authRoute);

rootRoute.use("/user", verifyToken, userRoute);

rootRoute.use("/comment", verifyToken, commentRoute);

rootRoute.use("/work", verifyToken, workRoute);

rootRoute.use("/work-type", verifyToken, workTypeRoute);

rootRoute.use("/work-type-detail", verifyToken, workTypeDetailRoute);

rootRoute.use("/hire-work", verifyToken, hireWorkRoute);

module.exports = rootRoute;
