const express = require("express");
const {
  getCommentList,
  createComment,
  editComment,
  deleteComment,
  getCommentById,
} = require("../controller/commentController");

const commentRoute = express.Router();

commentRoute.get("", getCommentList);
commentRoute.post("", createComment);
commentRoute.put("/:id", editComment);
commentRoute.delete("/:id", deleteComment);
commentRoute.get("/:id", getCommentById);

module.exports = commentRoute;
