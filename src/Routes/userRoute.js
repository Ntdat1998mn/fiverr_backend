const express = require("express");
const {
  getUserList,
  createUser,
  deleteUser,
  getUserById,
  editUser,
  getUserListByName,
  userSearchPagination,
} = require("../controller/userController");
const userRoute = express.Router();

userRoute.get("", getUserList);
userRoute.post("", createUser);
userRoute.delete("", deleteUser);
userRoute.get("/user-search-pagination", userSearchPagination);
userRoute.get("/:id", getUserById);
userRoute.put("/:id", editUser);
userRoute.get("/search/:name", getUserListByName);

module.exports = userRoute;
