const express = require("express");
const {
  getWorkList,
  createWork,
  getWorkById,
  editWork,
  deleteWork,
  getWorkTypeMenu,
  getWorkTypeDetailByWorkTypeGroupId,
  getWorkByWorkTypeDetailId,
  getWorkListByName,
  workSearchPagination,
} = require("../controller/workController");

const workRoute = express.Router();

workRoute.get("", getWorkList);
workRoute.post("", createWork);
workRoute.get("/work-search-pagination", workSearchPagination); // Nếu đặt dưới getWorkById thì sẽ lầm work-type-menu == id
workRoute.get("/work-type-menu", getWorkTypeMenu); // Nếu đặt dưới getWorkById thì sẽ lầm work-type-menu == id
workRoute.get("/:id", getWorkById);
workRoute.put("/:id", editWork);
workRoute.delete("/:id", deleteWork);
workRoute.get("/work-type-detail/:id", getWorkTypeDetailByWorkTypeGroupId);
workRoute.get("/work-by-work-type-detail-id/:id", getWorkByWorkTypeDetailId);
workRoute.get("/work-list/:name", getWorkListByName);

module.exports = workRoute;
