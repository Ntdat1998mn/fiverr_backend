const express = require("express");
const {
  getWorkDetailList,
  createWorkTypeDetail,
  getWorkTypeDetailById,
  editWorkTypeDetail,
  deleteWorkTypeDetail,
  createGroupWorkTypeDetail,
  editGroupWorkTypeDetail,
  workTypeDetailSearchPagination,
} = require("../controller/workTypeDetailController");

const workTypeDetailRoute = express.Router();

workTypeDetailRoute.get("", getWorkDetailList);
workTypeDetailRoute.post("", createWorkTypeDetail);
workTypeDetailRoute.get(
  "/work-type-detail-search-pagination",
  workTypeDetailSearchPagination
);
workTypeDetailRoute.get("/:id", getWorkTypeDetailById);
workTypeDetailRoute.put("/:id", editWorkTypeDetail);
workTypeDetailRoute.delete("/:id", deleteWorkTypeDetail);
workTypeDetailRoute.post("/type-detail-group", createGroupWorkTypeDetail);
workTypeDetailRoute.put("/type-detail-group/:id", editGroupWorkTypeDetail);

module.exports = workTypeDetailRoute;
