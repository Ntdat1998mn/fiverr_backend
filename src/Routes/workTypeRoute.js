const express = require("express");
const {
  getWorkTypeList,
  createWorkType,
  getWorkTypeById,
  editWorkType,
  deleteWorkType,
  workTypeSearchPagination,
} = require("../controller/workTypeController");

const workTypeRoute = express.Router();

workTypeRoute.get("", getWorkTypeList);
workTypeRoute.post("", createWorkType);
workTypeRoute.get("/work-type-search-pagination", workTypeSearchPagination);
workTypeRoute.get("/:id", getWorkTypeById);
workTypeRoute.put("/:id", editWorkType);
workTypeRoute.delete("/:id", deleteWorkType);

module.exports = workTypeRoute;
