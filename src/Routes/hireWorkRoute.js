const express = require("express");
const {
  getHirdeWorkList,
  hireWork,
  getHiredWorkById,
  editHiredWork,
  deleteHiredWork,
  getHirdeWorkListByUserId,
  hiredWorkSearchPagination,
} = require("../controller/hireWorkController");
const hireWorkRoute = express.Router();

hireWorkRoute.get("", getHirdeWorkList);
hireWorkRoute.post("", hireWork);
hireWorkRoute.get("/hire-work-list-by-user-id", getHirdeWorkListByUserId);
hireWorkRoute.get("/hire-work-ssearch-pgination", hiredWorkSearchPagination);
hireWorkRoute.get("/:id", getHiredWorkById);
hireWorkRoute.put("/:id", editHiredWork);
hireWorkRoute.delete("/:id", deleteHiredWork);

module.exports = hireWorkRoute;
