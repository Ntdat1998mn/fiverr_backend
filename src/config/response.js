const successCode = (res, data, message) => {
  res.status(200).json({
    data,
    message,
  });
};
const failCode = (res, message) => {
  res.status(400).json({
    message,
  });
};
const errorCode = (res, message) => {
  res.status(500).json({
    message,
  });
};

module.exports = {
  successCode,
  failCode,
  errorCode,
};
