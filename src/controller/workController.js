const { PrismaClient } = require("@prisma/client");
const { successCode, errorCode, failCode } = require("../config/response");
const prisma = new PrismaClient();

// Lấy danh sách công việc
const getWorkList = async (req, res) => {
  try {
    let data = await prisma.CongViec.findMany();
    if (data.length > 0) {
      successCode(res, data, "Lấy danh sách công việc thành công!");
    } else {
      failCode(res, "Không có dữ liệu!");
    }
  } catch (err) {
    errorCode(res, err.message);
  }
};

// Tạo công việc
const createWork = async (req, res) => {
  try {
    let { id: nguoiTao } = req.user.content;
    let {
      tenCongViec,
      danhGia,
      giaTien,
      hinhAnh,
      moTa,
      moTaNgan,
      saoCongViec,
      maChiTietLoaiCongViec,
    } = req.body;
    let user = await prisma.NguoiDung.findFirst({ where: { id: nguoiTao } });
    let workTypeDetail = await prisma.ChiTietLoaiCongViec.findFirst({
      where: { id: maChiTietLoaiCongViec },
    });
    let model = {
      nguoiTao,
      tenCongViec,
      danhGia,
      giaTien,
      hinhAnh,
      moTa,
      moTaNgan,
      saoCongViec,
      maChiTietLoaiCongViec,
    };
    if (user && workTypeDetail) {
      await prisma.CongViec.create({ data: model });
      successCode(res, model, "Thêm công việc thành công!");
    } else {
      failCode(res, "Người dùng hoặc chi tiết loại công việc không tồn tại!");
    }
  } catch (err) {
    errorCode(res, err.message);
  }
};

// Phân trang tìm kiếm công việc
const workSearchPagination = async (req, res) => {
  try {
    let { pageIndex, pageSize, keyWork } = req.body;
    let data = await prisma.CongViec.findMany({
      where: { tenCongViec: { contains: `${keyWork}` } },
      skip: (pageIndex - 1) * pageSize,
      take: pageSize,
    });
    if (data.length > 0) {
      successCode(res, data, "Lấy công việc thành công!");
    } else {
      failCode(res, "Công việc không tồn tại!");
    }
  } catch (err) {
    errorCode(res, err.message);
  }
};

// Lấy công việc theo mã công việc
const getWorkById = async (req, res) => {
  try {
    let id = req.params.id * 1;
    let data = await prisma.CongViec.findFirst({ where: { id } });
    if (data) {
      successCode(res, data, "Lấy công việc thành công!");
    } else {
      failCode(res, "Công việc không tồn tại!");
    }
  } catch (err) {
    errorCode(res, err.message);
  }
};

// Sửa công việc theo mã công việc
const editWork = async (req, res) => {
  try {
    let id = req.params.id * 1;
    let {
      tenCongViec,
      danhGia,
      giaTien,
      hinhAnh,
      moTa,
      moTaNgan,
      saoCongViec,
      maChiTietLoaiCongViec,
      nguoiTao,
    } = req.body;
    let model = {
      tenCongViec,
      danhGia,
      giaTien,
      hinhAnh,
      moTa,
      moTaNgan,
      saoCongViec,
      maChiTietLoaiCongViec,
    };
    let user = await prisma.NguoiDung.findFirst({ where: { id: nguoiTao } });
    let workTypeDetail = await prisma.ChiTietLoaiCongViec.findFirst({
      where: { id: maChiTietLoaiCongViec },
    });
    if (user && workTypeDetail) {
      await prisma.CongViec.update({
        data: model,
        where: { id },
      });
      successCode(res, model, "Chỉnh sửa công việc thành công!");
    } else {
      failCode(res, "Người dùng hoặc chi tiết loại công việc không tồn tại!");
    }
  } catch (err) {
    if (err.message.includes("not found")) {
      const error = new Error("Công việc không tồn tại!");
      failCode(res, error.message);
    } else {
      errorCode(res, err.message);
    }
  }
};

// Xoá công việc theo mã công việc
const deleteWork = async (req, res) => {
  try {
    let id = req.params.id * 1;
    let data = await prisma.CongViec.delete({ where: { id } });
    successCode(res, data, "Xoá công việc thành công!");
  } catch (err) {
    if (err.message.includes("not found")) {
      const error = new Error("Công việc không tồn tại!");
      failCode(res, error.message);
    } else {
      errorCode(res, err.message);
    }
  }
};

// Upload hình công việc theo mã công việc

// Lấy menu loại công việc
const getWorkTypeMenu = async (req, res) => {
  try {
    let data = await prisma.LoaiCongViec.findMany({
      include: { NhomChiTietLoai: { include: { ChiTietLoaiCongViec: true } } },
    });
    if (data.length > 0) {
      successCode(res, data, "Lấy menu loại công việc thành công!");
    } else {
      failCode(res, "Không có dữ liệu!");
    }
  } catch (err) {
    errorCode(res, err.message);
  }
};

// Lấy chi tiết loại công việc theo mã nhóm chi tiết loại công việc
const getWorkTypeDetailByWorkTypeGroupId = async (req, res) => {
  try {
    let id = req.params.id * 1;
    let data = await prisma.NhomChiTietLoai.findFirst({
      where: { id },
      include: { ChiTietLoaiCongViec: true },
    });
    if (data) {
      successCode(
        res,
        data,
        "Lấy chi tiết loại công việc theo mã nhóm chi tiết loại công việc thành công!"
      );
    } else {
      failCode(res, "Không có dữ liệu!");
    }
  } catch (err) {
    errorCode(res, err.message);
  }
};

// Lấy công viêc theo chi tiết loại công việc (mã chi tiết loại công việc)
const getWorkByWorkTypeDetailId = async (req, res) => {
  try {
    let id = req.params.id * 1;
    let data = await prisma.ChiTietLoaiCongViec.findFirst({
      where: { id },
      include: { CongViec: true },
    });
    if (data) {
      successCode(
        res,
        data,
        "Lấy công viêc theo chi tiết loại công việc thành công!"
      );
    } else {
      failCode(res, "Không có dữ liệu!");
    }
  } catch (err) {
    errorCode(res, err.message);
  }
};

// Lấy danh sách công việc theo tên công việc
const getWorkListByName = async (req, res) => {
  try {
    let tenCongViec = req.params.name;
    let data = await prisma.CongViec.findMany({
      where: { tenCongViec: { contains: `%${tenCongViec}%` } },
    });
    if (data.length > 0) {
      successCode(res, data, "Lấy danh sách công việc theo tên thành công!");
    } else {
      failCode(res, "Không có dữ liệu!");
    }
  } catch (err) {
    errorCode(res, err.message);
  }
};

module.exports = {
  getWorkList,
  createWork,
  getWorkById,
  editWork,
  deleteWork,
  getWorkTypeMenu,
  getWorkTypeDetailByWorkTypeGroupId,
  getWorkByWorkTypeDetailId,
  getWorkListByName,
  workSearchPagination,
};
