const { PrismaClient } = require("@prisma/client");
const { successCode, errorCode, failCode } = require("../config/response");
const prisma = new PrismaClient();

// Lấy danh sách công việc được thuê
const getHirdeWorkList = async (req, res) => {
  try {
    let data = await prisma.ThueCongViec.findMany();
    if (data.length > 0) {
      successCode(res, data, "Lấy danh sách công việc được thuê thành công!");
    } else {
      failCode(res, "Không có dữ liệu!");
    }
  } catch (err) {
    errorCode(res, err.message);
  }
};

// Thuê công việc
const hireWork = async (req, res) => {
  try {
    let { id: maNguoiThue } = req.user.content;
    let { maCongViec, hoanThanh } = req.body;
    let user = await prisma.NguoiDung.findFirst({ where: { id: maNguoiThue } });
    let work = await prisma.CongViec.findFirst({
      where: { id: maCongViec },
    });
    let model = { maCongViec, maNguoiThue, hoanThanh };
    if (user && work) {
      await prisma.ThueCongViec.create({ data: model });
      successCode(res, model, "Thuê công việc thành công!");
    } else {
      failCode(res, "Người dùng hoặc công việc được thuê không tồn tại!");
    }
  } catch (err) {
    errorCode(res, err.message);
  }
};

// Tìm kiếm thuê công việc (Phân trang tìm kiếm)
const hiredWorkSearchPagination = async (req, res) => {
  try {
    let { pageIndex, pageSize, keyWork } = req.body;
    let data = await prisma.$queryRaw`SELECT *
              FROM (
                SELECT ThueCongViec.id as id_ThueCongViec, ThueCongViec.maCongViec,
              ThueCongViec.maNguoiThue, ThueCongViec.ngayThue, ThueCongViec.hoanThanh, CongViec.tenCongViec
                FROM ThueCongViec
                JOIN CongViec ON ThueCongViec.maCongViec = CongViec.id
                LIMIT ${pageSize} OFFSET ${(pageIndex - 1) * pageSize}
              ) AS Page
              WHERE tenCongViec LIKE ${`%${keyWork}%`};`;
    if (data.length > 0) {
      successCode(res, data, "Lấy thuê công việc thành công!");
    } else {
      failCode(res, "Thuê công việc không tồn tại!");
    }
  } catch (err) {
    errorCode(res, err.message);
  }
};

// Lấy thuê công việc theo id
const getHiredWorkById = async (req, res) => {
  try {
    let id = req.params.id * 1;
    let data = await prisma.ThueCongViec.findFirst({ where: { id } });
    if (data) {
      successCode(res, data, "Lấy thuê công việc thành công!");
    } else {
      failCode(res, "Công việc được thuê không tồn tại!");
    }
  } catch (err) {
    errorCode(res, err.message);
  }
};

// Sửa thông tin thuê công việc
const editHiredWork = async (req, res) => {
  try {
    let id = req.params.id * 1;
    let { maCongViec, maNguoiThue, ngayThue, hoanThanh } = req.body;
    let model = {
      maCongViec,
      maNguoiThue,
      ngayThue,
      hoanThanh,
    };
    let user = await prisma.NguoiDung.findFirst({ where: { id: maNguoiThue } });
    let work = await prisma.CongViec.findFirst({
      where: { id: maCongViec },
    });
    if (user && work) {
      await prisma.ThueCongViec.update({
        data: model,
        where: { id },
      });
      successCode(res, model, "Chỉnh sửa công việc được thuê thành công!");
    } else {
      failCode(res, "Người dùng hoặc công việc không tồn tại!");
    }
  } catch (err) {
    if (err.message.includes("not found")) {
      const error = new Error("Công việc đươc thuê không tồn tại!");
      failCode(res, error.message);
    } else {
      errorCode(res, err.message);
    }
  }
};

// Xoá công việc đã thuê
const deleteHiredWork = async (req, res) => {
  try {
    let id = req.params.id * 1;
    let data = await prisma.ThueCongViec.delete({
      where: { id },
    });
    successCode(res, data, "Xoá công việc được thuê thành công!");
  } catch (err) {
    if (err.message.includes("not found")) {
      const error = new Error("Công việc được thuê không tồn tại!");
      failCode(res, error.message);
    } else {
      errorCode(res, err.message);
    }
  }
};

// Lấy danh sách công việc đã thuê cua người dùng
const getHirdeWorkListByUserId = async (req, res) => {
  try {
    let { id: maNguoiThue } = req.user.content;
    let data = await prisma.ThueCongViec.findMany({
      where: { maNguoiThue },
    });
    if (data.length > 0) {
      successCode(
        res,
        data,
        "Lấy danh sách công việc được thuê của người dùng thành công!"
      );
    } else {
      failCode(res, "Không có dữ liệu!");
    }
  } catch (err) {
    errorCode(res, err.message);
  }
};

module.exports = {
  getHirdeWorkList,
  hireWork,
  getHiredWorkById,
  editHiredWork,
  deleteHiredWork,
  getHirdeWorkListByUserId,
  hiredWorkSearchPagination,
};
