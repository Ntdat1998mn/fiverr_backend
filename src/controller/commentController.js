const { PrismaClient } = require("@prisma/client");
const { successCode, errorCode, failCode } = require("../config/response");
const prisma = new PrismaClient();

// Lấy bình luận
const getCommentList = async (req, res) => {
  try {
    let data = await prisma.BinhLuan.findMany();
    if (data.length > 0) {
      successCode(res, data, "Lấy danh sách bình luận thành công!");
    } else {
      failCode(res, "Không có dữ liệu!");
    }
  } catch (err) {
    errorCode(res, err.message);
  }
};

// Đăng bình luận
const createComment = async (req, res) => {
  try {
    let { id: maNguoiBinhLuan } = req.user.content;
    let { maCongViec, noiDung, saoBinhLuan } = req.body;
    let user = await prisma.NguoiDung.findFirst({
      where: { id: maNguoiBinhLuan },
    });
    let work = await prisma.CongViec.findFirst({ where: { id: maCongViec } });
    let model = { maCongViec, maNguoiBinhLuan, noiDung, saoBinhLuan };
    if (user && work) {
      await prisma.BinhLuan.create({ data: model });
      successCode(res, model, "Đăng bình luận thành công!");
    } else {
      failCode(res, "Người dùng hoặc công việc không tồn tại!");
    }
  } catch (err) {
    if (err.message.includes("not found")) {
      const error = new Error("Người dùng hoặc công việc không tồn tại!");
      failCode(res, error.message);
    } else {
      errorCode(res, err.message);
    }
  }
};

// Chỉnh sửa bình luận
const editComment = async (req, res) => {
  try {
    let id = req.params.id * 1;
    let { noiDung, saoBinhLuan } = req.body;
    let comment = await prisma.BinhLuan.findFirst({ where: { id } });
    let model = { noiDung, saoBinhLuan };
    if (comment) {
      let data = await prisma.BinhLuan.update({
        where: {
          id,
        },
        data: model,
      });

      if (data) {
        successCode(res, model, "Chỉnh sửa bình luận thành công!");
      } else {
        failCode(res, "Đã có lỗi, thử lại sau!");
      }
    } else {
      failCode(res, "Bình luận không tồn tại!");
    }
  } catch (err) {
    errorCode(res, err.message);
  }
};

// Xoá bình luận
const deleteComment = async (req, res) => {
  try {
    let id = req.params.id * 1;
    let data = await prisma.BinhLuan.delete({
      where: {
        id,
      },
    });
    successCode(res, data, "Xoá bình luận thành công!");
  } catch (err) {
    if (err.message.includes("not found")) {
      const error = new Error("Bình luận không tồn tại!");
      failCode(res, error.message);
    } else {
      errorCode(res, err.message);
    }
  }
};

// Lấy bình luận theo mã công việc
const getCommentById = async (req, res) => {
  try {
    let id = req.params.id * 1;
    let data = await prisma.BinhLuan.findFirst({ where: { id } });
    if (data) {
      successCode(res, data, "Lấy bình luận thành công!");
    } else {
      failCode(res, "Không có dữ liệu!");
    }
  } catch (err) {
    errorCode(res, err.message);
  }
};

module.exports = {
  getCommentList,
  createComment,
  editComment,
  deleteComment,
  getCommentById,
};
